﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mcphekt1___Sweepstakes.Models;

namespace mcphekt1___Sweepstakes.Controllers
{
    public class RegistrationController : Controller
    {
        private const int MAX_HORSES_PER_MEMBER = 3;
        //This is a constant
        private string[] DROP_DOWN_VALUES = { "One", "Two", "Three" };
        //
        // GET: /Registration/

        private SweepstakesDBContext context = new SweepstakesDBContext();
        private Random rand = new Random();

        [HttpGet]
        public ActionResult RegistrationForm()
        {
            //Select only the horses that do not have members assigned to them
            IEnumerable<Horse> availableHorses = from h in context.Horses
                                                 where h.AssignedMember == null
                                                 select h;

            //If there are no horses available then show the correct form
            if (availableHorses.Count() == 0)
            {
                return View("NoHorses");
            }
            else
            {
                List<SelectListItem> numberOfHorses = GetHorseDropDown(availableHorses);

                //Add the drop down values to the ViewBag
                ViewBag.NumberOfHorses = numberOfHorses;

                return View();
            }
        }

        private List<SelectListItem> GetHorseDropDown(IEnumerable<Horse> availableHorses)
        {
            //Create a list to be the values for the horse drop down
            List<SelectListItem> numberOfHorses = new List<SelectListItem>();

            //Add the options to the dropdown
            for (int i = 1; i <= MAX_HORSES_PER_MEMBER; i++)
            {
                //If there are horses available
                if (availableHorses.Count() >= i)
                {
                    //Add the item to the drop down list
                    numberOfHorses.Add(new SelectListItem { Text = DROP_DOWN_VALUES[i - 1], Value = i.ToString() });
                }
            }
            return numberOfHorses;
        }

        [HttpPost]
        public ActionResult RegistrationForm(Member ru)
        {
            //Check if any of the entered values were invalid
            if (!ModelState.IsValid)
            {
                //Get the available horses from the database
                IEnumerable<Horse> availableHorses = from h in context.Horses
                                                     where h.AssignedMember == null
                                                     select h;

                List<SelectListItem> numberOfHorses = GetHorseDropDown(availableHorses);

                //Add the drop down values to the ViewBag
                ViewBag.NumberOfHorses = numberOfHorses;

                return View();
            }
            else //The model was valid
            {
                //Get the randomly assigned horses for the member
                ru.AssignedHorses = GetHorses(ru.NumberOfHorses);

                //Add the member to the database
                context.Members.Add(ru);

                //Save the changes to the database
                context.SaveChanges();

                return View("Success", ru);
            }
        }

        //Gets a list of horses
        private List<Horse> GetHorses(int numHorses)
        {
            //Get a list of horses that have no members
            List<Horse> available = (from h in context.Horses
                                    where h.AssignedMember == null
                                    select h).ToList();

            //Create an empty list to hold the horses to be assigned to the member
            List<Horse> chosen = new List<Horse>();

            //Loop for the number if requested horses
            for (int i = 0; i < numHorses; i++)
            {
                //Get a random number to index into the available horses array
                int index = rand.Next(available.Count());
                //Add the horse at index to the chosen list
                chosen.Add(available[index]);

                //Remove the horse from the available list
                available.RemoveAt(index);
            }

            return chosen;
        }

    }
}
