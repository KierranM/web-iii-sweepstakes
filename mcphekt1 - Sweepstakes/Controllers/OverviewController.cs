﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mcphekt1___Sweepstakes.Models;

namespace mcphekt1___Sweepstakes.Controllers
{
    public class OverviewController : Controller
    {
        //
        // GET: /Overview/

        private SweepstakesDBContext context = new SweepstakesDBContext();

        public ActionResult ShowOverview()
        {
            List<Horse> horses = context.Horses.ToList();
            List<Member> members = context.Members.ToList();
            AllDataContainer container = new AllDataContainer { AllHorses = horses, AllMembers = members };
            

            return View(container);
        }

    }
}
