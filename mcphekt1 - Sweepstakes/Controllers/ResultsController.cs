﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mcphekt1___Sweepstakes.Models;

namespace mcphekt1___Sweepstakes.Controllers
{
    public class ResultsController : Controller
    {
        private const double FIRST_PLACE_WINNINGS = 0.5;
        private const double SECOND_PLACE_WINNINGS = 0.25;
        private const double THIRD_PLACE_WINNINGS = 0.15;
        private const double LAST_PLACE_WINNINGS = 0.10;
        private const int NUMBER_OF_HORSES = 12;
        private SweepstakesDBContext context = new SweepstakesDBContext();
        //
        // GET: /Results/

        [HttpGet]
        public ActionResult ResultsForm()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DisplayResults()
        {
            //Get a list of the horses in the race ordered by its finishing position
            List<Horse> orderdHorses = context.Horses.OrderBy(h => h.FinishingPosition).ToList();
            
            //Add the ordered horses to the viewbag
            ViewBag.OrderedHorses = orderdHorses;

            //Get a list of horses that have been assigned to a member from the ordered horses list
            List<Horse> assignedHorses = orderdHorses.Where(h => h.AssignedMember != null).ToList();

            //Calculate the total pool of the sweepsteaks by multiplting the number of assigned horses
            double totalPool = assignedHorses.Count() * 10;

            //Add the total pool to the viewbag
            ViewBag.TotalPool = totalPool;

            
            //Create a dictionary mapping a Member to the amount of money they won
            Dictionary<Member, double> memberWinnings = new Dictionary<Member, double>();

            //Add the members to the dictionary with an initial winnings amount of zero
            foreach (Member member in context.Members)
            {
                memberWinnings.Add(member, 0);
            }

            
            //If there were at least 1 horse that was assigned a member
            if (assignedHorses.Count > 0)
            {
                //If only one horse was assigned
                if (assignedHorses.Count == 1)
                {
                    //The member the horse was assigned to takes the whole pool
                    memberWinnings[assignedHorses[0].AssignedMember] += totalPool;
                }
                else //There was more than one horse assigned
                {
                    //If two horses were assigned a member
                    if (assignedHorses.Count == 2)
                    {
                        //The horse that came first gets the amount equal to the first and second place prizes
                        memberWinnings[assignedHorses[0].AssignedMember] += ((totalPool * FIRST_PLACE_WINNINGS) + (totalPool * SECOND_PLACE_WINNINGS));
                        //The horse that came second gets the amount equal to the third and last place prizes
                        memberWinnings[assignedHorses[1].AssignedMember] += ((totalPool * THIRD_PLACE_WINNINGS) + (totalPool * LAST_PLACE_WINNINGS));
                    }
                    else //There was more than two horses asssigned
                    {
                        //If three horses were assigned a member
                        if (assignedHorses.Count == 3)
                        {
                            //Split the money three ways, add the 10% from last place as a third to each value
                            memberWinnings[assignedHorses[0].AssignedMember] += totalPool * FIRST_PLACE_WINNINGS + (totalPool * 0.04);
                            memberWinnings[assignedHorses[1].AssignedMember] += totalPool * SECOND_PLACE_WINNINGS + (totalPool * 0.03);
                            memberWinnings[assignedHorses[2].AssignedMember] += totalPool * THIRD_PLACE_WINNINGS + (totalPool * 0.03);
                        }
                        else // more than three horses assigned
                        {
                            //Addign the winnings to the appropriate members
                            memberWinnings[assignedHorses[0].AssignedMember] += totalPool * FIRST_PLACE_WINNINGS;
                            memberWinnings[assignedHorses[1].AssignedMember] += totalPool * SECOND_PLACE_WINNINGS;
                            memberWinnings[assignedHorses[2].AssignedMember] += totalPool * THIRD_PLACE_WINNINGS;
                            //The last place horse gets the last place prize
                            memberWinnings[assignedHorses.Last().AssignedMember] += totalPool * LAST_PLACE_WINNINGS;
                        }//End multiple horses else
                    }// end two horses else
                }//end one horse else
            }// end no assigned horses if

            //Order the memberWinnings dictionary by the amount of money they one
            memberWinnings = (memberWinnings.OrderByDescending(m => m.Value)).ToDictionary(m => m.Key, m => m.Value);

            ViewBag.MemberWinnings = memberWinnings;

            return View("DisplayResults");
        }
    }
}
