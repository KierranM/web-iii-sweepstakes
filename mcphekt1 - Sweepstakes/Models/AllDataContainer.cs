﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mcphekt1___Sweepstakes.Models
{
    public class AllDataContainer
    {
        public List<Horse> AllHorses { get; set; }
        public List<Member> AllMembers { get; set; }
    }
}