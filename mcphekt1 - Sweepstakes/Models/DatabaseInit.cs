﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace mcphekt1___Sweepstakes.Models
{
    public class DatabaseInit : DropCreateDatabaseAlways<SweepstakesDBContext>
    {
        protected override void Seed(SweepstakesDBContext context)
        {
            //Seed the horses
            foreach (Horse horse in GetSeedHorses())
            {
                context.Horses.Add(horse);
            }
            //Don't seed any members

            //Save the context
            context.SaveChanges();
        }

        private List<Horse> GetSeedHorses()
        {
            List<Horse> horses = new List<Horse>
            {
                new Horse { Name = "GLASS HARMONIUM",   Form = "13X66X10X1", FinishingPosition =  4 },
                new Horse { Name = "DUNADEN",           Form = "12X3X12X91", FinishingPosition = 1 },
                new Horse { Name = "DRUNKEN SAILOR",    Form = "X446X13137", FinishingPosition = 12 },
                new Horse { Name = "MANIGHAR",          Form = "2573X34844", FinishingPosition = 5 },
                new Horse { Name = "UNUSUAL SUSPECT",   Form = "3621X5X406", FinishingPosition = 9 },
                new Horse { Name = "FOX HUNT",          Form = "2164112461", FinishingPosition = 7 },
                new Horse { Name = "LUCAS CRANACH",     Form = "111X4215X5", FinishingPosition = 3 },
                new Horse { Name = "PRECEDENCE",        Form = "X025X67009", FinishingPosition = 11 },
                new Horse { Name = "RED CADEAUX",       Form = "9X20141053", FinishingPosition = 2 },
                new Horse { Name = "LOST IN THE MOMENT",Form = "189X223274", FinishingPosition = 6 }, 
                new Horse { Name = "AT FIRST SIGHT",    Form = "23245X0622", FinishingPosition = 10 },
                new Horse { Name = "NIWOT",             Form = "0121X07201", FinishingPosition = 8 }
            };

            return horses;
        }
    }
}