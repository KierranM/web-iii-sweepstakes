﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace mcphekt1___Sweepstakes.Models
{
    public class Horse
    {
        public int HorseID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Form { get; set; }

        public int FinishingPosition { get; set; }
        
        //Relationship
        public virtual Member AssignedMember { get; set; }

    }
}