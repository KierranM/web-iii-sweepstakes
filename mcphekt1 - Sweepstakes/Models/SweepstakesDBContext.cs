﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace mcphekt1___Sweepstakes.Models
{
    public class SweepstakesDBContext : DbContext
    {
        public DbSet<Member> Members { get; set; }
        public DbSet<Horse> Horses { get; set; }
    }
}