﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace mcphekt1___Sweepstakes.Models
{
    public class Member
    {
        //Table ID
        public int MemberID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        //This regex enforces a slightly modified version of the RFC 5322 found at http://www.regular-expressions.info/email.html
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage="Please enter a valid email address")]
        public string Email { get; set; }
        [Required]
        [RegularExpression("[1-3]", ErrorMessage="Must be a number")]
        public int NumberOfHorses { get; set; }

        public List<Horse> AssignedHorses { get; set; }
    }
}